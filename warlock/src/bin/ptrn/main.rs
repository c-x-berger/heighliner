use std::{
    io::Read,
    net::Shutdown,
    os::unix::net::UnixStream,
    path::{Path, PathBuf},
};

use anyhow::Context;
use clap::Parser;

use warlock::{Message, Response};

#[derive(Parser)]
#[clap(version, name = "ptrn", about = "boss warlocks around")]
struct Options {
    /// Name of the daemon to control.
    ///
    /// By default, looks for a socket named /var/run/warlock/<name>, but this can be changed with
    /// --socket-directory.
    #[clap(name = "DAEMON", required = true)]
    daemon: PathBuf,
    #[clap(long, default_value = "/var/run/warlock/")]
    /// Change the directory to look for control sockets in.
    ///
    /// This is just haphazardly pasted in front of the daemon name.
    socket_directory: PathBuf,
    #[clap(short, long)]
    /// Print the PID of the daemon.
    ///
    /// Note that by the time this command returns, the daemon may have been restarted for some
    /// reason, invalidating the returned PID value.
    say_pid: bool,
    #[clap(short = 'r', long)]
    /// Enable or disable automatically restarting this daemon.
    ///
    /// By default, daemons are automatically restarted according to their Pact. This allows
    /// overriding that behavior without also stopping or starting the daemon. If automatic
    /// restarting is enabled, and the daemon is not already running, it will immediately start.
    set_restart: Option<bool>,
    #[clap(short, long, conflicts_with("set-restart"))]
    /// Get whether or not automatic restarting is enabled.
    ///
    /// Print "true" if automatic restarting is enabled, or "false" if it isn't.
    get_restart: bool,
    #[clap(short, long)]
    /// Start the daemon.
    up: bool,
    #[clap(short, long, conflicts_with = "up")]
    /// Stop the daemon.
    ///
    /// Send the daemon SIGTERM, followed by SIGCONT (in case it is paused). As a side effect, this
    /// also disables automatic restarting. If for some reason you want to just stop the daemon
    /// without changing its restart setting, you can just send those signals with the -c and -t
    /// flags.
    down: bool,
    #[clap(short, long)]
    /// Send the daemon SIGSTOP, pausing it.
    pause: bool,
    #[clap(short, long = "continue")]
    /// Send the daemon SIGCONT, reversing the effect of SIGSTOP.
    cont: bool,
    #[clap(short, long)]
    /// Send the daemon SIGINT.
    interrupt: bool,
    #[clap(short, long)]
    /// Send the daemon SIGTERM.
    terminate: bool,
    #[clap(short, long)]
    /// Send the daemon SIGKILL.
    kill: bool,
    #[clap(short, long, parse(from_occurrences))]
    verbose: usize,
}

impl Options {
    fn daemon(&self) -> &Path {
        self.daemon.as_ref()
    }

    fn socket_directory(&self) -> &Path {
        self.socket_directory.as_ref()
    }

    fn create_messages(&self) -> Vec<Message> {
        let mut messages = Vec::with_capacity(1);
        // horrible yandev code
        if self.say_pid {
            messages.push(Message::SayPid);
        }
        if self.up {
            messages.push(Message::Start);
        } else if self.down {
            messages.push(Message::Stop);
            messages.push(Message::SetRestart(false));
        }
        if self.pause {
            messages.push(Message::Pause);
        }
        if self.cont {
            messages.push(Message::Continue);
        }
        if self.interrupt {
            messages.push(Message::Interrupt);
        }
        if self.terminate {
            messages.push(Message::Terminate);
        }
        if self.kill {
            messages.push(Message::Kill);
        }
        if self.get_restart {
            messages.push(Message::GetRestart);
        }
        if let Some(restart) = self.set_restart {
            messages.push(Message::SetRestart(restart))
        }
        messages
    }
}

fn main() -> anyhow::Result<()> {
    let options = Options::parse();

    let socket_path = {
        let mut p = PathBuf::from(options.socket_directory());
        p.push(options.daemon());
        p
    };

    let mut sock = UnixStream::connect(&socket_path).with_context(|| {
        format!(
            "Could not open daemon control socket ({})",
            socket_path.to_string_lossy()
        )
    })?;
    serde_lexpr::to_writer(&mut sock, &options.create_messages())
        .context("Could not write to socket")?;
    sock.shutdown(Shutdown::Write)?;

    let response = {
        let mut buf = vec![];
        sock.read_to_end(&mut buf)
            .context("Reading response failed")?;
        buf
    };
    if options.verbose > 0 {
        println!("{}", String::from_utf8_lossy(&response));
    }

    let response: Vec<Response> = serde_lexpr::from_slice(&response)?;
    dbg!(response);

    Ok(())
}
