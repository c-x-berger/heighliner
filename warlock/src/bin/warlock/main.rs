use std::{fs::File, path::PathBuf};

use anyhow::Context;
use clap::Parser;
use tokio::{net::UnixListener, signal, sync::mpsc};

use warlock::{pact::error::MessageHandlingError, Pact};

mod handlers;

const BACKLOG: usize = 16;

#[derive(Parser)]
struct Options {
    /// Filename of pact to run.
    #[clap(parse(from_os_str), value_name = "PACT")]
    pact: PathBuf,
    /// Filename of socket to create and bind to. If not specified, /var/run/warlock/[name] is used
    /// instead, where [name] is the the file stem of the pact file (the filename up to the first
    /// dot in the common case).
    #[clap(parse(from_os_str), value_name = "SOCKET")]
    sock_path: Option<PathBuf>,
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    let options = Options::parse();
    println!("Hello, world!");

    let stem = options.pact.file_stem().context("expected a file name")?;

    // either argv[2] or /var/run/warlock/[name]
    let sock_path = match options.sock_path {
        Some(path) => PathBuf::from(path),
        None => {
            let mut p = PathBuf::from("/var/run/warlock/");
            p.push(stem);
            p
        }
    };
    let listener = UnixListener::bind(&sock_path)
        .with_context(|| format!("binding to {}", sock_path.to_string_lossy()))?;
    let stem = stem.to_string_lossy();

    let mut pact: Pact = serde_lexpr::from_reader(
        File::open(&options.pact)
            .with_context(|| format!("opening pact-file {:?}", options.pact))?,
    )?;
    println!("starting {}", stem);
    let mut daemon = Some(pact.start()?);

    let (tx, mut rx) = mpsc::channel(BACKLOG);

    loop {
        // TODO: DRY nonsense. Maybe futures::future::select_all()?
        daemon = if let Some(ref mut d) = daemon {
            tokio::select! {
                res = listener.accept() => {
                    let (stream, _) = res?;
                    tokio::spawn(handlers::on_connection(stream, tx.clone()));
                    daemon
                },
                exit = d.await_end() => {
                    let exit = exit?;
                    eprintln!("{} exited: {}", stem, exit);
                    if pact.restart {
                        Some(pact.start()?)
                    } else {
                        None
                    }
                }
                msg = rx.recv() => {
                    if let Some((msg, tx)) = msg {
                        let reply = handlers::on_message(&mut daemon, &mut pact, msg).await;
                        tx.send(reply).map_err(|_| MessageHandlingError::MainHungUp)?;
                        daemon
                    } else {
                        break;
                    }
                },
                _ = signal::ctrl_c() => break,
            }
        } else {
            tokio::select! {
                res = listener.accept() => {
                    let (stream, _) = res?;
                    tokio::spawn(handlers::on_connection(stream, tx.clone()));
                    daemon
                },
                msg = rx.recv() => {
                    if let Some((msg, tx)) = msg {
                        let reply = handlers::on_message(&mut daemon, &mut pact, msg).await;
                        tx.send(reply).map_err(|_| MessageHandlingError::MainHungUp)?;
                        daemon
                    } else {
                        break;
                    }
                },
                _ = signal::ctrl_c() => break,
            }
        };
        if daemon.is_none() && pact.restart {
            daemon = Some(pact.start()?);
        }
    }

    tokio::fs::remove_file(sock_path).await?;
    // don't leave garbage in the terminal
    print!("");

    Ok(())
}
