use std::time::Duration;

use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::UnixStream,
    sync::{mpsc, oneshot},
};

use warlock::{
    daemon::Daemon, pact::error::MessageHandlingError, Message, MessageResponse, Pact, PactMessage,
    Response,
};

pub(super) async fn on_connection(
    mut stream: UnixStream,
    messenger: mpsc::Sender<PactMessage>,
) -> anyhow::Result<()> {
    let parsed_messages: anyhow::Result<Vec<Message>> = {
        let mut buf = vec![];
        let timeout =
            tokio::time::timeout(Duration::from_secs(2), stream.read_to_end(&mut buf)).await;

        match timeout {
            // timeout elapsed
            Err(e) => Err(e.into()),
            // turn io error into anyerr, else if ok attempt to parse (turning parse err into anyerr)
            Ok(io_res) => io_res
                .map_err(|e| e.into())
                .and_then(|_| serde_lexpr::from_slice(&buf).map_err(|e| e.into())),
        }
    };
    match parsed_messages {
        Err(_) => {
            let response: Vec<Result<(), _>> = vec![Err(MessageHandlingError::BadRequest)];
            stream
                .write(serde_lexpr::to_vec(&response)?.as_slice())
                .await?;
        }
        Ok(parsed) => {
            let mut responses: Vec<Response> = Vec::with_capacity(parsed.len());
            for message in parsed {
                let response = {
                    let (reply_tx, reply_rx) = oneshot::channel();
                    messenger.send((message, reply_tx)).await?;
                    reply_rx.await?
                };
                responses.push(response);
            }
            stream
                .write(serde_lexpr::to_vec(&responses)?.as_slice())
                .await?;
        }
    }
    Ok(())
}

// executed by the main task when a background task supplies messages to handle
// the background task can't do it because you need mut access to the Daemon which means doing
// locks and what and this is so much more fun
pub(super) async fn on_message(
    daemon: &mut Option<Daemon>,
    pact: &mut Pact,
    message: Message,
) -> Response {
    match message {
        Message::SayPid => match daemon.as_ref().and_then(|d| d.id()) {
            Some(id) => Ok(MessageResponse::Pid(id)),
            None => Err(MessageHandlingError::DaemonGone),
        },
        Message::GetRestart => Ok(MessageResponse::Restart(pact.restart)),
        Message::SetRestart(restart) => {
            pact.restart = restart;
            Ok(MessageResponse::Restart(pact.restart))
        }
        Message::Start => match daemon {
            Some(_) => Ok(MessageResponse::Started),
            None => {
                *daemon = Some(pact.start()?);
                Ok(MessageResponse::Started)
            }
        },
        Message::Stop => match daemon {
            Some(ref mut d) => d
                .stop()
                .map(|_| MessageResponse::Stopped)
                .map_err(|e| e.into()),
            None => Err(MessageHandlingError::DaemonGone),
        },
        m @ (Message::Pause
        | Message::Continue
        | Message::Interrupt
        | Message::Terminate
        | Message::Kill) => daemon
            .as_ref()
            .map_or(Err(MessageHandlingError::DaemonGone), |d| {
                let signal = m.try_into().unwrap();
                d.signal(Some(signal))?;
                Ok(signal.into())
            }),
    }
}
