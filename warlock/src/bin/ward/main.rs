use std::{
    collections::HashMap,
    ffi::OsString,
    path::{Path, PathBuf},
};

use anyhow::{anyhow, Context};
use clap::Parser;
use nix::{
    sys::{signal, signal::Signal},
    unistd,
    unistd::Pid,
};
use notify::{
    event::{ModifyKind, RenameMode},
    Event, EventKind, RecursiveMode, Watcher,
};
use thiserror::Error;
use tokio::{
    process::Command,
    select,
    sync::{mpsc, oneshot},
};

#[derive(Debug)]
enum DaemonEvent {
    /// Reload the warlock instance (because e.g. the pact file changed on disk)
    Reload,
    /// Stop this warlock instance entirely.
    Shutdown,
    WardShutdown(oneshot::Sender<()>),
}

#[derive(Parser)]
#[clap(name = "ward", about = "Warlock Directory")]
struct Options {
    #[clap(name = "DIRECTORY", required = true)]
    /// Directory to scan for pacts.
    directory: PathBuf,
    #[clap(short = 'P')]
    /// Create a new session and process group for each warlock process.
    ///
    /// See setsid(2).
    create_process_group: bool,
}

#[derive(Error, Debug)]
enum WatcherTaskError {
    #[error("intermittent IO error")]
    Io(#[from] std::io::Error),
    #[error("task supervising a warlock hung up")]
    WarlockHungUp(#[from] mpsc::error::SendError<DaemonEvent>),
}

impl Options {
    pub fn directory(&self) -> &Path {
        self.directory.as_path()
    }
}

/// Background task to supervise one warlock process. If `setsid` is `true`, `ward` will call
/// `setsid(2)` after `fork(2)` to put the warlock process into its own session and process group.
async fn manage_warlock(
    pact: PathBuf,
    mut rx: mpsc::Receiver<DaemonEvent>,
    setsid: bool,
) -> anyhow::Result<()> {
    let mut warlock = Command::new("warlock");
    warlock.arg(pact.as_os_str());
    let mut sock_path = OsString::from("sockets/");
    sock_path.push(pact.file_name().unwrap());
    warlock.arg(sock_path);
    if setsid {
        // Yuck. I think this is mostly just std (and by extension Tokio) being paranoid about
        // pre_exec, but what can you do.
        unsafe {
            warlock.pre_exec(|| {
                unistd::setsid()
                    .map(|_| ())
                    // holy shit, this *works*?
                    .map_err(|errno| std::io::Error::from_raw_os_error(errno as i32))
            });
        }
    }
    let mut child = warlock.spawn()?;
    return loop {
        let pid = child.id().expect("warlock failed to start?");
        select! {
            biased;

            event = rx.recv() => {
                signal::kill(Pid::from_raw(pid.try_into().unwrap()), Signal::SIGINT).with_context(|| format!("signalling {}", pid))?;
                // if we get None, the main loop hung up and we should die
                let event = event.context("recv'ing ward event")?;
                match event {
                    DaemonEvent::Shutdown => break Ok(()),
                    DaemonEvent::Reload => continue,
                    DaemonEvent::WardShutdown(tx) => {
                        // main calls join_all on the receiver, so this should always work
                        tx.send(()).unwrap();
                        break Ok(());
                    }
                }
            },
            exit  = child.wait() => {
                let exit = exit.expect("warlock failed to start");
                eprintln!("{pid} exited: {exit}, respawning");
                dbg!(warlock.as_std());
                child = warlock.spawn()?;
            },
        }
    };
}

/// Create and start a background task to manage a warlock process. See `manage_warlock` for details. Returns
/// the mpsc Sender to communicate with the task.
async fn create_warlock(pact: PathBuf, setsid: bool) -> mpsc::Sender<DaemonEvent> {
    let (tx, rx) = mpsc::channel(16);
    println!("starting warlock on {:?}", pact);
    tokio::spawn(manage_warlock(pact, rx, setsid));
    tx
}

// man, what a type signature
/// Send an event to a warlock instance. Returns `None` if no warlock exists in the `warlocks` map,
/// `Some(Err)` if the warlock existed but the event could not be sent, or `Some(Ok)` if the event
/// was sent successfully.
async fn send_event<P: AsRef<Path>>(
    warlock: P,
    event: DaemonEvent,
    warlocks: &mut HashMap<PathBuf, mpsc::Sender<DaemonEvent>>,
) -> Option<Result<(), mpsc::error::SendError<DaemonEvent>>> {
    // futures are values, lol
    let future = warlocks.get(warlock.as_ref()).map(|tx| tx.send(event));
    if let Some(future) = future {
        Some(future.await)
    } else {
        None
    }
}

/// Background task to process filesystem events from `notify`. Events arrive on the `events` mpsc
/// channel and are processed in a loop.
async fn event_handler(
    event: Event,
    warlocks: &mut HashMap<PathBuf, mpsc::Sender<DaemonEvent>>,
    setsid: bool,
) -> Result<(), WatcherTaskError> {
    let path = event.paths.get(0).expect("event concerned no file paths?!");
    match event.kind {
        EventKind::Any => eprintln!("got Any event type - no idea what to do with that..."),
        EventKind::Other => {
            eprintln!("got Other event type - no idea what to do with that...")
        }
        EventKind::Access(_) => (),
        EventKind::Create(_) => {
            if std::fs::metadata(path)?.is_file() {
                let tx = create_warlock(path.to_path_buf(), setsid).await;
                warlocks.insert(path.to_path_buf(), tx);
            }
        }
        EventKind::Modify(ModifyKind::Data(_)) => {
            send_event(path, DaemonEvent::Reload, warlocks).await;
        }
        EventKind::Modify(ModifyKind::Name(mode)) => {
            handle_rename(mode, event.paths, warlocks, setsid).await?;
        }
        EventKind::Modify(_) => (),
        EventKind::Remove(_) => {
            send_event(path, DaemonEvent::Shutdown, warlocks).await;
            warlocks.remove(path);
        }
    }
    Ok(())
}

async fn handle_rename(
    mode: RenameMode,
    mut paths: Vec<PathBuf>,
    warlocks: &mut HashMap<PathBuf, mpsc::Sender<DaemonEvent>>,
    setsid: bool,
) -> Result<(), WatcherTaskError> {
    match mode {
        RenameMode::From => {
            let res = send_event(&paths[0], DaemonEvent::Shutdown, warlocks).await;
            if let Some(r) = res {
                warlocks.remove(&paths[0]);
                r?
            }
        }
        RenameMode::To => {
            let path = paths
                .pop()
                .expect("RenameMode::To should always include a path");
            if !warlocks.contains_key(&path) {
                let tx = create_warlock(path.clone(), setsid).await;
                warlocks.insert(path, tx);
            }
        }
        RenameMode::Both => {
            // we'll probably mostly ignore these events by the time all is said and done, btu
            // maybe BSD's kqueue only provides Both events somehow. i doubt it but who knows, not
            // me
            let new_name = paths.pop().expect("RenameMode::Both should have both");
            if !warlocks.contains_key(&new_name) {
                let create = create_warlock(new_name.clone(), setsid);
                let shutdown = send_event(&paths[0], DaemonEvent::Shutdown, warlocks);
                let (create, _) = tokio::join!(create, shutdown);
                warlocks.insert(new_name, create);
            } else {
                send_event(&paths[0], DaemonEvent::Shutdown, warlocks).await;
            }
            warlocks.remove(&paths[0]);
        }
        _ => todo!(),
    }
    dbg!(mode, warlocks);
    Ok(())
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    let options = Options::parse();
    let mut warlocks: HashMap<PathBuf, mpsc::Sender<DaemonEvent>> = HashMap::new();
    for file in std::fs::read_dir(options.directory())? {
        let file = file?;
        // file.path() is a very sneaky copy
        let tx = create_warlock(file.path(), options.create_process_group).await;
        warlocks.insert(file.path(), tx);
    }

    let (tx, mut rx) = mpsc::channel(16);

    let mut watcher = notify::recommended_watcher(move |event| match event {
        Ok(event) => tx.blocking_send(event).expect("event handler task hung up"),
        Err(e) => eprintln!("intermittent IO error: {:?}", e),
    })?;
    watcher
        .watch(options.directory(), RecursiveMode::NonRecursive)
        .with_context(|| {
            format!(
                "watching directory {}",
                options.directory().to_string_lossy()
            )
        })?;

    println!("now waiting for notify events...");
    loop {
        tokio::select! {
            event = rx.recv() => {
                // notify events
                if let Some(event) = event {
                    event_handler(event, &mut warlocks, options.create_process_group).await?;
                } else {
                    eprintln!("Uh, the notify thread hung up on us. Dying.");
                    break;
                }
            },
            _ = tokio::signal::ctrl_c() => {
                let mut shutdowns = Vec::with_capacity(warlocks.len());
                for (p, tx) in warlocks.iter() {
                    let (stx, srx) = oneshot::channel();
                    let event = DaemonEvent::WardShutdown(stx);
                    tx.send(event).await.with_context(|| format!("notifying {} of shutdown", p.to_string_lossy()))?;
                    shutdowns.push(srx);
                }
                futures::future::join_all(shutdowns).await;
                break;
            }
        }
    }

    Err(anyhow!("broke main loop"))
}
