use nix::{sys::signal::Signal, unistd::Pid};
use serde::{Deserialize, Serialize};
use tokio::sync::{mpsc, oneshot};

pub mod daemon;
pub mod pact;

pub use pact::Pact;

// TODO: This should be something waay more structured.
pub type DaemonName = String;
/// Either the successful result of handling a message or an error that
/// (hopefully) explains what went wrong.
pub type Response = Result<MessageResponse, pact::error::MessageHandlingError>;
/// The actual internal messages sent between tasks. The oneshot sender is used
/// to send back the result of handling the message.
pub type PactMessage = (Message, oneshot::Sender<Response>);
/// Type alias used to simplify spelling out the type of the mpsc channel.
pub type PactChannel = mpsc::Sender<PactMessage>;

/// Type to represent an individual requested operation.
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub enum Message {
    /// Fetch the current PID of the daemon.
    SayPid,
    /// Fetch whether the daemon will restart automatically or not.
    GetRestart,
    /// Set whether  the daemon will (not) automatically restart on exiting.
    SetRestart(bool),
    /// Attempt to start the daemon.
    Start,
    /// Attempt to stop the daemon.
    Stop,
    /// Send the special `SIGSTOP` signal to the daemon, pausing its execution.
    Pause,
    /// Undoes `Pause`.
    Continue,
    /// Send `SIGINT`.
    Interrupt,
    /// Send `SIGTERM`.
    Terminate,
    /// Send `SIGKILL`.
    Kill,
}

/// The successful result of handling a `Message`.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum MessageResponse {
    /// The PID of the daemon.
    Pid(i32),
    /// Whether the daemon automatically restarts.
    Restart(bool),
    /// Successfully started.
    Started,
    /// Successfully stopped.
    Stopped,
    /// Sent a signal with the given signal number (e.g. 15 for SIGTERM).
    /// Technically this representation means if you created a `Signal(n)` on
    /// one OS, it may not represent the same thing on another OS if one party
    /// is doing something like running Linux on an N64, but then you'd be
    /// sending these across hosts and not a local Unix socket which is probably
    /// not correct.
    Signal(i32),
}

impl From<Pid> for MessageResponse {
    fn from(pid: Pid) -> Self {
        Self::Pid(pid.as_raw())
    }
}

impl From<Signal> for MessageResponse {
    fn from(signal: Signal) -> Self {
        Self::Signal(signal as i32)
    }
}

impl TryFrom<Message> for nix::sys::signal::Signal {
    type Error = String;

    fn try_from(message: Message) -> Result<Self, Self::Error> {
        match message {
            Message::Pause => Ok(Signal::SIGSTOP),
            Message::Continue => Ok(Signal::SIGCONT),
            Message::Interrupt => Ok(Signal::SIGINT),
            Message::Terminate => Ok(Signal::SIGTERM),
            Message::Kill => Ok(Signal::SIGKILL),
            _ => Err("not a signalling message".into()),
        }
    }
}
