use std::path::{Path, PathBuf};

use serde::Deserialize;

use crate::daemon::{error::StartDaemonError, Daemon};

mod deser;
pub mod error;

/// A user-defined "pact", representing how to start a daemon and other information about it.
#[derive(Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Pact {
    #[serde(deserialize_with = "deser::deserialize_cmdlist")]
    cmd: Vec<String>,
    stdout: Option<PathBuf>,
    stderr: Option<PathBuf>,
    pub restart: bool,
}

impl Pact {
    /// The command to start this daemon as a slice of strings. This will always
    /// have at least one element - the only way to construct a `Pact` is via
    /// serde, which uses a custom deserializer here to ensure the list is
    /// non-empty.
    pub fn cmd(&self) -> &[String] {
        &self.cmd
    }

    /// Starts the daemon defined by this pact. Note that no attempt is made to prevent "duplicate"
    /// daemons - this will *always* try to create a new process (akin to Command::spawn).
    pub fn start(&self) -> Result<Daemon, StartDaemonError> {
        Daemon::start(&self)
    }

    /// The name of the file to redirect standard output to.
    pub fn stdout(&self) -> Option<&Path> {
        self.stdout.as_deref()
    }

    /// The name of the file to redirect standard error to.
    pub fn stderr(&self) -> Option<&Path> {
        self.stderr.as_deref()
    }
}
