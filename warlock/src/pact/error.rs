use std::io;

use serde::{Deserialize, Serialize};
use thiserror::Error;

use crate::daemon::error::{StartDaemonError, StopDaemonError};

/// Things that can go wrong when handling messages (i.e. actually when actually
/// controlling the running daemon).
#[derive(Error, Debug, Serialize, Deserialize)]
pub enum MessageHandlingError {
    /// The daemon was not running when we needed it to be (e.g. to send it a
    /// signal).
    #[error("daemon was stopped but should have been live")]
    DaemonGone,
    /// The daemon could not be stopped for some reason.
    #[error("could not stop daemon")]
    Unstoppable,
    /// The daemon could not be (re)started.
    #[error("could not start daemon")]
    Unstartable,
    /// There was a communication error between the "main" task and the
    /// background tasks responsible for handling socket connections.
    #[error("main loop hung up on pact")]
    MainHungUp,
    #[error("could not parse user request")]
    BadRequest,
    #[error("badly categorized I/O error")]
    Io,
    #[error("*NIX error: {0}")]
    Nix(i32),
}

impl From<nix::Error> for MessageHandlingError {
    fn from(e: nix::Error) -> Self {
        Self::Nix(e as i32)
    }
}

// TODO: I don't use the nice thiserror macros to generate this and *really*
// should. The blocker is making StartDaemonError/StopDaemonError Serialize,
// which could be doable if the inner [io::Error]s there are converted (albiet
// lossily) into something else.
//
// because these are ultimately sent back to the user, including more info about
// the io error - even if it means annoyingly passing around strings as "errors"
// - seems desirable
impl From<StartDaemonError> for MessageHandlingError {
    fn from(_: StartDaemonError) -> Self {
        Self::Unstartable
    }
}

impl From<StopDaemonError> for MessageHandlingError {
    fn from(_: StopDaemonError) -> Self {
        Self::Unstoppable
    }
}

impl From<io::Error> for MessageHandlingError {
    fn from(_: io::Error) -> Self {
        Self::Io
    }
}
