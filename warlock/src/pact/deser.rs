use std::{ffi::OsStr, fmt, fmt::Formatter, marker::PhantomData};

use serde::{
    de,
    de::{SeqAccess, Visitor},
    Deserialize, Deserializer,
};

struct CmdListVisitor<T>(PhantomData<fn() -> T>);

impl<'de, T> Visitor<'de> for CmdListVisitor<T>
where
    T: Deserialize<'de> + AsRef<OsStr>,
{
    type Value = Vec<T>;

    fn expecting(&self, fmt: &mut Formatter) -> fmt::Result {
        fmt.write_str("a nonempty list of stringy things")
    }

    fn visit_seq<S>(self, mut seq: S) -> Result<Self::Value, S::Error>
    where
        S: SeqAccess<'de>,
    {
        let mut v = vec![];
        let e = seq.next_element()?.ok_or_else(|| {
            de::Error::custom("no values in sequence when constructing command arguments")
        })?;
        v.push(e);
        while let Some(e) = seq.next_element()? {
            v.push(e);
        }
        Ok(v)
    }
}

pub(super) fn deserialize_cmdlist<'de, T, D>(deserializer: D) -> Result<Vec<T>, D::Error>
where
    T: Deserialize<'de> + AsRef<OsStr>,
    D: Deserializer<'de>,
{
    let visitor = CmdListVisitor(PhantomData);
    deserializer.deserialize_seq(visitor)
}
