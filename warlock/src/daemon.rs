use std::{
    fs::OpenOptions,
    io,
    path::Path,
    process::{ExitStatus, Stdio},
};

use nix::{
    sys::{signal, signal::Signal},
    unistd::Pid,
};
use tokio::process::{Child, Command};

use crate::Pact;

pub mod error {
    use std::io;

    use thiserror::Error;

    /// Things that might go wrong when starting a daemon.
    #[derive(Error, Debug)]
    pub enum StartDaemonError {
        /// Technically, serde should guard us against this error, but in
        /// general we need to return something in the (impossible) case where
        /// e.g. we have an empty list for the command or something.
        #[error("despite it being impossible, the Pact was invalid")]
        InvalidPact,
        /// Could not set up standard input or output (e.g. because we could not
        /// create or open file for writing)
        #[error("could not redirect stdio")]
        Stdio(#[source] io::Error),
        /// Could not spawn the child process (e.g. specified an executable that
        /// isn't accessible to warlock)
        #[error("failed to spawn child process")]
        Spawning(#[source] io::Error),
    }

    /// Things that might go wrong when stopping a daemon.
    #[derive(Error, Debug)]
    pub enum StopDaemonError {
        #[error("daemon was stopped but should have been live")]
        DaemonGone,
        #[error("failed to signal daemon")]
        Io(#[from] nix::Error),
    }
}
use error::*;

#[derive(Debug)]
pub struct Daemon(Child);

impl Daemon {
    fn create_stdio<P>(dest: P) -> io::Result<Stdio>
    where
        P: AsRef<Path>,
    {
        let file = OpenOptions::new().append(true).create(true).open(dest)?;
        Ok(Stdio::from(file))
    }

    pub fn id(&self) -> Option<i32> {
        self.0
            .id()
            .map(|u| u.try_into().expect("PID somehow greater than i32::MAX?!"))
    }

    pub async fn await_end(&mut self) -> io::Result<ExitStatus> {
        self.0.wait().await
    }

    pub fn start(pact: &Pact) -> Result<Self, StartDaemonError> {
        let exec = pact.cmd();
        let cmd = exec.get(0).ok_or(StartDaemonError::InvalidPact)?;
        let mut process = Command::new(cmd);
        for arg in exec[1..].iter() {
            process.arg(arg);
        }
        // annoying DRY violation
        match pact.stdout() {
            Some(ref p) => {
                process.stdout(Self::create_stdio(p).map_err(|e| StartDaemonError::Stdio(e))?)
            }
            None => process.stdout(Stdio::inherit()),
        };
        match pact.stderr() {
            Some(ref p) => {
                process.stderr(Self::create_stdio(p).map_err(|e| StartDaemonError::Stdio(e))?)
            }
            None => process.stderr(Stdio::inherit()),
        };

        let child = process.spawn().map_err(|e| StartDaemonError::Spawning(e))?;
        Ok(Daemon(child))
    }

    /// Sends the signals to stop this Daemon. You still need to collect the exit status with
    /// `await_end`.
    pub fn stop(&self) -> Result<(), StopDaemonError> {
        match self.id() {
            Some(id) => {
                let pid = Pid::from_raw(id);
                signal::kill(pid, Signal::SIGTERM)?;
                signal::kill(pid, Signal::SIGCONT)?;
                Ok(())
            }
            None => Err(StopDaemonError::DaemonGone),
        }
    }

    pub fn signal<T>(&self, signal: T) -> nix::Result<()>
    where
        T: Into<Option<Signal>>,
    {
        if let Some(id) = self.id() {
            let pid = Pid::from_raw(id);
            signal::kill(pid, signal)
        } else {
            Ok(())
        }
    }
}
