use nom::{
    branch::alt,
    bytes::complete::is_not,
    character::complete::{char, multispace1},
    combinator::{map, value, verify},
    multi::{fold_many0, separated_list1},
    sequence::{delimited, preceded},
    IResult,
};

// various escape sequences
fn escaped_char<'a>(input: &'a str) -> IResult<&'a str, char> {
    preceded(
        char('\\'),
        alt((
            value('\\', char('\\')),
            value('\n', char('n')),
            value('\r', char('r')),
            value('\t', char('t')),
            value('"', char('"')),
        )),
    )(input)
}

// backslash, followed by any amount of whitespace
fn escaped_whitespace<'a>(input: &'a str) -> IResult<&'a str, &'a str> {
    preceded(char('\\'), multispace1)(input)
}

// non-empty block that doesn't contain \ or "
fn parse_literal<'a>(input: &'a str) -> IResult<&'a str, &'a str> {
    verify(is_not("\"\\"), |s: &str| !s.is_empty())(input)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum StringFragment<'a> {
    Literal(&'a str),
    EscapedChar(char),
    EscapedWs,
}

fn parse_fragment<'a>(input: &'a str) -> IResult<&'a str, StringFragment<'a>> {
    alt((
        map(parse_literal, StringFragment::Literal),
        map(escaped_char, StringFragment::EscapedChar),
        value(StringFragment::EscapedWs, escaped_whitespace),
    ))(input)
}

fn parse_string<'a>(input: &'a str) -> IResult<&'a str, String> {
    let build_string = fold_many0(parse_fragment, String::new, |mut string, fragment| {
        match fragment {
            StringFragment::Literal(s) => string.push_str(s),
            StringFragment::EscapedChar(c) => string.push(c),
            StringFragment::EscapedWs => {}
        }
        string
    });

    delimited(char('"'), build_string, char('"'))(input)
}

// non-empty block that doesn't contain whitespace or \
fn parse_word_literal<'a>(input: &'a str) -> IResult<&'a str, &'a str> {
    verify(is_not(" \t\r\n\\"), |s: &str| !s.is_empty())(input)
}

fn parse_word_fragment<'a>(input: &'a str) -> IResult<&'a str, StringFragment<'a>> {
    alt((
        map(parse_word_literal, StringFragment::Literal),
        map(escaped_char, StringFragment::EscapedChar),
    ))(input)
}

fn parse_word<'a>(input: &'a str) -> IResult<&'a str, String> {
    fold_many0(parse_word_fragment, String::new, |mut string, fragment| {
        match fragment {
            StringFragment::Literal(s) => string.push_str(s),
            StringFragment::EscapedChar(c) => string.push(c),
            StringFragment::EscapedWs => unreachable!(),
        }
        string
    })(input)
}

fn parse_both<'a>(input: &'a str) -> IResult<&'a str, String> {
    alt((parse_string, parse_word))(input)
}

pub(crate) fn parse_entry<'a>(input: &'a str) -> IResult<&'a str, Vec<String>> {
    separated_list1(char(' '), parse_both)(input)
}

#[cfg(test)]
mod test {
    use super::{
        escaped_char, escaped_whitespace, parse_both, parse_fragment, parse_literal, parse_string,
        parse_word_literal, StringFragment,
    };
    use nom::{
        error::{Error, ErrorKind},
        Err,
    };

    #[test]
    fn test_escaped_char() {
        let tests = vec![
            (r"\\", '\\'),
            (r"\n", '\n'),
            (r"\r", '\r'),
            (r"\t", '\t'),
            ("\\\"", '"'),
        ];
        for test in tests {
            println!("data   = \"{}\"\nexpect = {:?}", test.0, test.1);
            let result = escaped_char(&test.0);
            assert_eq!(result, Ok(("", test.1)));
        }
    }

    #[test]
    fn test_escaped_whitespace() {
        let tests = vec![
            (r"\ ", ("", " ")),
            ("\\\ttext", ("text", "\t")),
            ("\\\r\n\t", ("", "\r\n\t")),
        ];
        for test in tests {
            println!("data = \"{}\"\nexpect = {:?}", test.0, test.1);
            let result = escaped_whitespace(&test.0);
            assert_eq!(result, Ok(test.1));
        }
    }

    #[test]
    fn test_parse_literal() {
        let tests = vec![
            (
                "this is a lot of text that is all one literal",
                ("", "this is a lot of text that is all one literal"),
            ),
            (r"text\", (r"\", "text")),
            (r"contains\npingas", (r"\npingas", "contains")),
        ];
        for test in tests {
            println!("data = \"{}\"\nexpect = {:?}", test.0, test.1);
            let result = parse_literal(&test.0);
            assert_eq!(result, Ok(test.1));
        }
    }

    #[test]
    fn test_parse_word_literal() {
        let tests = vec![
            ("text", ("", "text")),
            ("t r", (" r", "t")),
            ("te xt", (" xt", "te")),
            (r"t\r", (r"\r", "t")),
        ];
        for test in tests {
            println!("data = \"{}\"\nexpect = {:?}", test.0, test.1);
            let result = parse_word_literal(&test.0);
            assert_eq!(result, Ok(test.1));
        }
    }

    #[test]
    fn test_parse_fragment() {
        let tests = vec![
            ("text", ("", StringFragment::Literal("text"))),
            (r"\ t", ("t", StringFragment::EscapedWs)),
            (r"\t", ("", StringFragment::EscapedChar('\t'))),
        ];
        for test in tests {
            println!("data = \"{}\"\nexpect = {:?}", test.0, test.1);
            let result = parse_fragment(&test.0);
            assert_eq!(result, Ok(test.1));
        }
    }

    #[test]
    fn test_parse_string() {
        // TODO: This is not an exhaustive or even very torturous list.
        let tests = vec![
            (r#""text""#, Ok(("", String::from("text")))),
            (r#""in"out"#, Ok(("out", String::from("in")))),
            (r#""text\t""#, Ok(("", String::from("text\t")))),
            (r#""r\ t""#, Ok(("", String::from("rt")))),
            ("nope", Err(Err::Error(Error::new("nope", ErrorKind::Char)))),
        ];
        for test in tests {
            println!("data = \"{}\"\nexpect = {:?}", test.0, test.1);
            let result = parse_string(&test.0);
            assert_eq!(result, test.1);
        }
    }

    #[test]
    fn test_parse_both() {
        // TODO: This is not an exhaustive or even very torturous list.
        let tests = vec![
            (r#""text""#, Ok(("", String::from("text")))),
            (r#""in"out"#, Ok(("out", String::from("in")))),
            (r#""text\t""#, Ok(("", String::from("text\t")))),
            (r#""r\ t""#, Ok(("", String::from("rt")))),
            ("\"r\\\nt\"", Ok(("", String::from("rt")))),
            ("nope", Ok(("", String::from("nope")))),
            ("\"nope", Ok(("", String::from("\"nope")))),
            ("t r", Ok((" r", String::from("t")))),
            ("in\"out\"", Ok(("", String::from("in\"out\"")))),
            (r#""text"out""#, Ok(("out\"", String::from("text")))),
            (
                "\"complex\\n\" text",
                Ok((" text", String::from("complex\n"))),
            ),
        ];
        for test in tests {
            println!("data = \"{}\"\nexpect = {:?}", test.0, test.1);
            let result = parse_both(&test.0);
            assert_eq!(result, test.1);
        }
    }
}
