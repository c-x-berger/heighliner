use std::{
    env,
    fs::File,
    io::{BufRead, BufReader},
    process::Command,
    thread,
    time::Duration,
};

use anyhow::Result;
use nix::{
    errno::Errno,
    libc::{prctl, PR_SET_CHILD_SUBREAPER},
    sys::{
        signal::Signal,
        signalfd::{SigSet, SignalFd},
        wait::{waitpid, WaitPidFlag, WaitStatus},
    },
    unistd::Pid,
};
use nom::Finish;

mod parser;

fn main() -> Result<()> {
    println!("Hello, world!");

    // SAFETY: all libc functions are unsafe, even when they simply set flags on a process like
    // prctl. This is 1:1 with C's prctl.
    unsafe {
        prctl(PR_SET_CHILD_SUBREAPER, 1, 0, 0, 0);
    }

    // set up signalfd **now** so if an initline dies quick we don't miss it
    let mut mask = SigSet::empty();
    mask.add(Signal::SIGCHLD);
    // prevent default handling
    mask.thread_block()?;
    let mask = mask;
    let mut sfd = SignalFd::new(&mask).expect("could not open signalfd - what?!");

    let filename = env::args()
        .nth(1)
        .unwrap_or_else(|| String::from("/etc/initlines"));
    let f = File::open(&filename)?;
    let f = BufReader::new(f);

    let mut commands = vec![];
    for (i, line) in f.lines().enumerate() {
        let lineno = i + 1;
        let line = line?;
        let line = line.trim();

        let parsed = parser::parse_entry(&line).finish();
        if let Ok(("", mut v)) = parsed {
            println!("entry: {:?}", v);
            v.reverse(); // v can now be a stack
            let program = match v.pop() {
                Some(s) if !s.is_empty() => s,
                Some(s) if s.is_empty() => {
                    eprintln!("{}:{}: exec path cannot be empty", filename, lineno);
                    continue;
                }
                Some(_) => unreachable!(),
                None => {
                    eprintln!("{}:{}: impossibly short entry on line", filename, lineno);
                    continue;
                }
            };
            let mut cmd = Command::new(program);
            while let Some(s) = v.pop() {
                cmd.arg(s);
            }
            commands.push(cmd.spawn()?);
        } else {
            match parsed {
                Ok((s, _)) => eprintln!("expected nothing left to parse on line {} but got {} instead! contact the spacing guild.", i, s),
                Err(e) => eprintln!("could not parse line {}: {}", i, e),
            }
        }
    }

    loop {
        if let Ok(Some(_)) = sfd.read_signal() {
            println!("got sigchld, reaping");
            // we got a SIGCHLD - reap until there's nothing left
            // we need to loop since signals "coalesce" - basically, the pending signals are a set
            // rather than a list
            loop {
                match waitpid(Pid::from_raw(-1), Some(WaitPidFlag::WNOHANG)) {
                    Ok(WaitStatus::Exited(p, _)) => {
                        println!("reaped process {}", p);
                    }
                    Ok(s) if s != WaitStatus::StillAlive => continue,
                    Ok(_) => break,
                    // Occurs when we have no children to reap - appears harmless.
                    Err(e) if e == Errno::ECHILD => {}
                    Err(e) => {
                        eprintln!("error when reaping zombies: {}", e);
                        break;
                    }
                }
            }
        }
        thread::sleep(Duration::from_secs(1));
    }
}
