benny
===
From [nih.gov][nih]:
> The most common anti-anxiety medications are called benzodiazepines.

`benny` is a minimal PID 1, designed to prevent kernel panics and... uh...

No, actually, that's it.

It can also spawn a series of executables to do the things you might normally
associate with PID 1 (reaping zombies, daemon management, etc).

use
---
Don't? This is amateur hour code.

[nih]: https://www.nimh.nih.gov/health/topics/mental-health-medications#part_2360
